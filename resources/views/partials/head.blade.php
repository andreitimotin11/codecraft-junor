<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>--}}
{{--    <link rel="stylesheet" type="text/css" media="screen"--}}
{{--          href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">--}}
    {{-- SEO --}}
    <title>@if(isset($meta['title'])){{ $meta['title']}}@endif</title>
    @if(isset($meta['description']) && !empty($meta['description']))
        <meta name="description" content="{{  $meta['description'] }}">
    @endif
    @if(isset($meta['keywords']) && !empty($meta['keywords']))
        <meta name="keywords" content="{{  $meta['keywords'] }}">
@endif

@stack('css')
</head>
