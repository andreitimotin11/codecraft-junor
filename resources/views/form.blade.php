<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Formular</title>
</head>
@extends('layouts.front')
@push('css')
{{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <body>
    <div class="container">
        <div class="wrapper col-sm-10">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h3>Contact us</h3>
            <form action="{{route("processForm")}}" method="post" class="form-horizontal">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="exampleInputEmail1" class="col-sm-3 col-form-label">Email address</label>
                    <div class="cols-sm-7">
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                              placeholder="Enter email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="examplePhone" class="col-sm-3 col-form-label">Phone</label>
                    <div class="cols-sm-7">
                        <input type="tel" name="phone" class="form-control" id="examplePhone" aria-describedby="phoneHelp"
                           placeholder="Enter phone">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputPassword1" class="col-sm-3 col-form-label">Password</label>
                    <div class="cols-sm-7">
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                              placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea2">Message</label>
                    <textarea name="message" class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control" />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Type</label>
                    <select multiple class="form-control" id="exampleFormControlSelect2">
                        <option>landing</option>
                        <option>business</option>
                        <option>online shop</option>
                    </select>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    @include('partials.common_scripts')

    </body>
@endsection
</html>
