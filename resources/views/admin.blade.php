<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Programare la dentist</title>
</head>
@extends('layouts.front')
@push('css')
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
    {{--        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>--}}
@endpush
@section('content')
    <div class="container">
        <div class="wrapper col-sm-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="appointment">
                <h3>Programare la dentist</h3>
                <form action="{{route("processAdminForm")}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="doctor" class="col-sm-3 col-form-label">Choose a doctor</label>
                        <div class="cols-sm-7">
                            <select class="form-control" name="doctor" id="doctor">
                                @foreach($doctors as $doctor)
                                    <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="procedure" class="col-sm-3 col-form-label">Choose a procedure</label>
                        <div class="cols-sm-7">

                            <select class="form-control" name="procedure" id="procedure">
                                @foreach($procedures as $procedure)
                                    <option value="{{$procedure->id}}">{{$procedure->name}} ({{$procedure->duration}}
                                        min)
                                    </option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="time" class="col-form-label">Time</label>
                        <div id="datetimepicker" class="input-append input-group date">
                            <input name="time" type="text" id="time">
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="schedule">
                <h3>Last procedures</h3>
                <form action="" method="post" class="form-horizontal">
                    <div class="form-group row">
                        <label for="select_doctor" class="col-sm-3 col-form-label">Choose a doctor</label>
                        <div class="cols-sm-7">
                            <select class="form-control" name="select_doctor" id="select_doctor">
                                @foreach($doctors as $doctor)
                                    <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
                <table id="schedule_table" class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Doctor</th>
                        <th scope="col">Procedure</th>
                        <th scope="col">Day</th>
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
    <script type="text/javascript">
        $('#datetimepicker').datetimepicker({
            format: 'dd.MM.yyyy hh:mm:ss'
        });

        $(document).ready(function () {
            let doctorId = $('#select_doctor').val();

            function ajax(doctorId) {
                $.ajax({
                    data: {'doctorId': doctorId},
                    method: "get",
                    url: "{{route('getSchedule')}}",
                    success: function (result) {
                        drawSchedule(result);
                    }
                });
            }

            ajax(doctorId);


            $('#select_doctor').on('change', (function change() {
                let doctorId = $('#select_doctor').val();
                ajax(doctorId);
            }));

            function drawSchedule(result) {
                let row = '';
                let table = $('#schedule_table > tbody');
                table.html(row);

                let i = 1;
                for (let data of result) {
                    row += `<tr>
    <td>${i++}</td>
    <td>${data.surname} ${data.name}</td>
    <td>${data.procedure_name}</td>
    <td>${data.date}</td>
    <td>${data.begin_at}</td>
    <td>${data.end_at}</td>
</tr>`;
                    table.html(row);
                }
            }
        });
    </script>
@endsection
</html>