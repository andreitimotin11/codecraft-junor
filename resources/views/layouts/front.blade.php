<!DOCTYPE html>
<html lang="en">
@include('partials.head')

<body>
@yield('content')

@include('partials.common_scripts')
@yield('scripts')
</body>
</html>