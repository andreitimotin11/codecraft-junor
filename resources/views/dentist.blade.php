<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Programare la dentist</title>
</head>
@extends('layouts.front')
@push('css')
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <div class="container">
        <div class="wrapper col-sm-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h3>Programare la dentist</h3>
            <form action="{{route("processDentistForm")}}" method="post" class="form-horizontal">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="procedure" class="col-sm-3 col-form-label">Choose a procedure</label>
                    <div class="cols-sm-9">

                        <select class="form-control" name="procedure" id="procedure">
                            @foreach($procedures as $procedure)
                                <option value="{{$procedure->id}}">{{$procedure->name}} ({{$procedure->duration}}
                                    min)
                                </option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group row">
                    <label for="doctor" class="col-sm-3 col-form-label">Choose a doctor</label>
                    <div class="cols-sm-9">
                        <select class="form-control" name="doctor" id="doctor">
                            @foreach($doctors as $doctor)
                                <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="time" class="col-form-label">Time</label>
                    <div id="datetimepicker2" class="input-append input-group date">
                        <input name="time" type="text" id="time">
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputEmail1" class="col-sm-3 col-form-label">Your name, surname</label>
                    <div class="cols-sm-9">
                        <input type="name" name="name" class="form-control" id="name"
                               placeholder="Enter your name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phoneNumber" class="col-sm-3 col-form-label">Your phone number</label>
                    <div class="cols-sm-9">
                        <input type="tel" name="phoneNumber" class="form-control" id="phoneNumber"
                               placeholder="Enter your phone number">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                    <div class="cols-sm-9">
                        <input type="email" name="email" class="form-control" id="email"
                               placeholder="Enter your email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="comments" class="col-sm-3 col-form-label">Message</label>
                    <div class="cols-sm-9">
                        <textarea name="comments" id="comments" cols="30" rows="5" ></textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript">
        $('#datetimepicker2').datetimepicker({
            format: 'dd.MM.yyyy hh:mm:ss'
        });
    </script>
@endsection
</html>
