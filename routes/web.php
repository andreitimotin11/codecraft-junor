<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/form', 'FormController@index');
Route::get('/dentist', 'FormController@dentist')->name('dentist');
Route::get('/admin', 'AdminController@setAppointment')->name('admin');
Route::post('/processForm', 'FormController@processForm')->name('processForm');
Route::post('/processDentistForm', 'FormController@processDentistForm')->name('processDentistForm');
Route::post('/processAdminForm', 'AdminController@processAdminForm')->name('processAdminForm');
Route::get('/getSchedule', 'AdminController@getSchedule')->name('getSchedule');
Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    return view('test_bootstrap_datetimepicker');
});
