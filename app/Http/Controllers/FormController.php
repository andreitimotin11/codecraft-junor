<?php

namespace App\Http\Controllers;

use App\Models\JsonFileSaver;
use App\Models\MessageManager;
use App\Models\TimeChecker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormController
{
    public function index()
    {
        return view('form', [
            'test' => 'test'
        ]);
    }
    
    public function dentist()
    {
        $doctors = DB::table('doctors')->get();
        $procedures = DB::table('procedures')->get();
//        $doctors = 1; todo: using model Doctors
        return view('dentist',
            [
                'doctors'    => $doctors,
                'procedures' => $procedures,
            ]);
    }
    public function processForm(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'email',
            'message' => 'required',
        ]);
        $data = $request->all();
        $saver = new JsonFileSaver('file.json');
        MessageManager::saveMessage($data, $saver);
    }
    
    public function processDentistForm(Request $request)
    {
        $request->validate([
            'doctor'    => 'required|int',
            'procedure' => 'required',
            'time'      => 'required',
        ]);
        $data = $request->post();
//    dd($data['time']);
        $dateTime = Carbon::parse($data['time']);
//        dd($dateTime);
        try {
            if (!TimeChecker::check($dateTime, $data['doctor'], $data['procedure'])) {
                DB::table('appointments')->insert(
                    [
                        'doctor_id'    => $data['doctor'],
                        'procedure_id' => $data['procedure'],
                        'date'         => $dateTime->format('Y-m-d'),
                        'begin_at'     => $dateTime->format('H:i:s'),
                    ]
                );
            }else{
                return back()->withErrors(['message' => 'The doctor is busy at the specified time!']);
            };
        } catch (\Exception $exception) {
            return back()->withErrors(['message' => $exception->getMessage()]);
        }
        
        return back();
    }
}