<?php

namespace App\Http\Controllers;

use App\Models\TimeChecker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController
{
    public function setAppointment()
    {
        $doctors = DB::table('doctors')->get();
        $procedures = DB::table('procedures')->get();
//        $doctors = 1; todo: using model Doctors
        return view('admin',
            [
                'doctors'    => $doctors,
                'procedures' => $procedures,
            ]);
    }
    
    public function processAdminForm(Request $request)
    {
        $request->validate([
            'doctor'    => 'required|int',
            'procedure' => 'required',
            'time'      => 'required',
        ]);
        $data = $request->post();
        
        $dateTime = Carbon::parse($data['time']);
//        dd($data['time']);
        if (!TimeChecker::check($dateTime, $data['doctor'], $data['procedure'])) {
            DB::table('appointments')->insert(
                [
                    'doctor_id'    => $data['doctor'],
                    'procedure_id' => $data['procedure'],
                    'date'         => $dateTime->format('Y-m-d'),
                    'begin_at'     => $dateTime->format('H:i:s'),
                ]
            );
        }else{
            return back()->withErrors(['message' => 'The doctor is busy at the specified time!']);
        };
        
        return back();
    }
    
    public function getSchedule(Request $request)
    {
        $doctor_id = $request->all();
        
        $appointments = DB::table('appointments')
            ->select('doctors.name', 'doctors.surname', 'date',
                DB::raw('procedures.name as procedure_name'),
                'appointments.begin_at', 'procedures.duration',
                DB::raw('ADDTIME(begin_at, SEC_TO_TIME(duration*60)) as end_at')
            )
            ->leftJoin('doctors', 'doctor_id', 'doctors.id')
            ->leftJoin('procedures', 'procedure_id', 'procedures.id')
            ->where('appointments.doctor_id', '=', $doctor_id)
            ->get();
        
        return $appointments;
    }
}