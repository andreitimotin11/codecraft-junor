<?php

namespace App\Models;

use DateTimeZone;
use Illuminate\Support\Facades\DB;

class TimeChecker
{
    public static function check(\DateTime $dateTime, int $doctorId, int $procedureType): bool
    {
//        var_dump($dateTime);
//        die(__CLASS__.' : '.__LINE__);
        $start = $dateTime->format('H:i:s');
//        $dateTime = new \DateTime(now(), new DateTimeZone('Europe/Kiev') );
//        dd($dateTime);
//        dd(new \DateTime(now(), new DateTimeZone('Europe/Kiev')) > $dateTime);
        if (new \DateTime(now(), new DateTimeZone('Europe/Kiev')) > $dateTime) {
            throw new \Exception('Appointment time is in the past!');
        }
        $date = $dateTime->format('Y-m-d');
        $end = self::getTimeForProcedure($dateTime, $procedureType);
    
        $result = DB::table('appointments')
            ->select('appointments.date', 'appointments.begin_at', 'appointments.procedure_id', 'procedures.duration',
                DB::raw('
                 CASE WHEN "'.$start.'" BETWEEN begin_at AND
                 ADDTIME(begin_at, SEC_TO_TIME(duration*60))
                THEN 1
                 WHEN "' . $end . '" BETWEEN begin_at AND
                 ADDTIME(begin_at, SEC_TO_TIME(duration*60))
                THEN 1
                WHEN begin_at BETWEEN "'.$start.'" AND
                 "'.$end.'"
                THEN 1
                WHEN ADDTIME(begin_at, SEC_TO_TIME(duration*60)) BETWEEN "'.$start.'" AND
                 "'.$end.'"
                THEN 1
                ELSE 0 END as control')
            )->leftJoin('procedures', 'procedure_id', 'procedures.id')
//            ->leftJoin('doctors', 'doctor_id', 'doctors.id')
            ->where('appointments.date', '=', $date)
            ->where('doctor_id', $doctorId)
            ->having('control', '=', 1)
            ->get();
        return count($result) > 0;
    }
    
    private static function getTimeForProcedure(\DateTime $start, int $procedureType)
    {
         $time = DB::table('procedures')
            ->where('procedures.id', $procedureType)
            ->get(['duration'])->first();
         $result = $start->add(date_interval_create_from_date_string($time->duration . "minutes"));
        return $result->format('H:i:s');
    }
}