<?php

namespace App\Models;

use App\Contracts\SaverInterface;

class JsonFileSaver implements SaverInterface
{
    private $filename;
    
    /**
     * JsonFileSaver constructor.
     * @param $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }
    
    public function save($data): bool
    {
        try {
            file_put_contents($this->filename, json_encode($data));
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }
    
}