<?php

namespace App\Models;

class MessageManager
{
    public static function saveMessage($data, JsonFileSaver $saver ): bool
    {
        $saver->save($data);
        return true;
    }
}