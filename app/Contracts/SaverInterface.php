<?php

namespace App\Contracts;

interface SaverInterface
{
    public function save($data): bool ;
}